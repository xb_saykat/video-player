using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ColorMixC : MonoBehaviour
{
   [SerializeField]
    public GameObject gameObjectName;
    private Image image;
    public Button button;
    private Color applyColor,buttonColor;
    private int count= 0;
   
    void Start()
    {
        image = gameObjectName.GetComponent<Image>();
        button.onClick.AddListener(ChangeSphereColor);
        //gameObject.GetComponent<Button>().onClick.AddListener(ChangeSphereColor);
        
    }

    public void ChangeSphereColor(){
        count++;
        if(count%2==1){
        //Button Selected

        Debug.Log("C "+image.color);
        Color blueColor;
        ColorUtility.TryParseHtmlString("#3866AF",out blueColor);
        
        Color redColor;
        ColorUtility.TryParseHtmlString("#E41525",out redColor);
        
        if(image.color.Equals(blueColor))
        {
            ColorUtility.TryParseHtmlString("#00A549",out applyColor);//Green
            
        }else if(image.color.Equals(redColor)){
            ColorUtility.TryParseHtmlString("#F39417",out applyColor);//giya
            
        }else{
             ColorUtility.TryParseHtmlString("#FFEA2E",out applyColor);//yellow
            
        }
        image.color= applyColor;
        ColorUtility.TryParseHtmlString("#FFFFFF",out buttonColor);
        button.GetComponent<Image>().color = buttonColor;
        
       }else{
        //Button Unselected
        ColorUtility.TryParseHtmlString("#FFFFFF",out applyColor);
        image.color= applyColor;

        ColorUtility.TryParseHtmlString("#FFEA2E",out buttonColor);
        button.GetComponent<Image>().color = buttonColor;

        }
     Debug.Log("B "+image.color);
    }

   
    void Update()
    {
      
    }
}
