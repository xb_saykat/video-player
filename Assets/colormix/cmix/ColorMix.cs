using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ColorMix : MonoBehaviour
{
   [SerializeField]
    public GameObject gameObjectName;
    private Image image;
    public Button button;
    private Color applyColor,buttonColor,whiteColor;
    private int count= 0;
    bool shaking = false;
    public Animator animator;
  
    void Start()
    {
        image = gameObjectName.GetComponent<Image>();
        ColorUtility.TryParseHtmlString("#E41525",out whiteColor);
       // var imageCanvas = GameObject.FindByTag("ImageOfCanvas");
        button.onClick.AddListener(ChangeSphereColor);
    }

    public void ChangeSphereColor(){
        count++;
        if(count%2==1){
        //Button Selected
        ColorUtility.TryParseHtmlString("#E41525",out applyColor);
        image.color= applyColor;

        ColorUtility.TryParseHtmlString("#FFFFFF",out buttonColor);
        button.GetComponent<Image>().color = buttonColor;

       }else{
        //Button Unselected
        ColorUtility.TryParseHtmlString("#FFFFFF",out applyColor);
        image.color= applyColor;

        ColorUtility.TryParseHtmlString("#E41525",out buttonColor);
        button.GetComponent<Image>().color = buttonColor;

        }
        Debug.Log("A "+image.color);
    }

    void Update(){
         if(image.color.Equals(whiteColor)){
            Debug.Log("XYZ "+image.color);
            //Shake();
         }
    }

}
