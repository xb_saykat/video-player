using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ColorMixB : MonoBehaviour
{
   [SerializeField]
    public GameObject gameObjectName;
    private Image image;
    public Button button;
    private Color applyColor,buttonColor;
    private int count= 0;
   
    void Start()
    {
      image = gameObjectName.GetComponent<Image>();
      button.onClick.AddListener(ChangeSphereColor);
     // gameObject.GetComponent<Button>().onClick.AddListener(ChangeSphereColor);
        
    }

    public void ChangeSphereColor(){
    count++;
        if(count%2==1){
        //Button Selected

        Debug.Log("B "+image.color);
        Color redColor;
        ColorUtility.TryParseHtmlString("#E41525",out redColor);

        if(image.color.Equals(redColor)){
          ColorUtility.TryParseHtmlString("#5E2D87",out applyColor);
        }else{
          ColorUtility.TryParseHtmlString("#3866AF",out applyColor);
 
        }
        image.color= applyColor;
        ColorUtility.TryParseHtmlString("#FFFFFF",out buttonColor);
        button.GetComponent<Image>().color = buttonColor;
        
       }else{
        //Button Unselected
        ColorUtility.TryParseHtmlString("#FFFFFF",out applyColor);
        image.color= applyColor;

        ColorUtility.TryParseHtmlString("#3866AF",out buttonColor);
        button.GetComponent<Image>().color = buttonColor;

        }
     Debug.Log("B "+image.color);
       
    }

   
    void Update()
    {
        
    }
}
