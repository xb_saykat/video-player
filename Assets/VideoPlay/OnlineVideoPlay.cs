using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
 
public class OnlineVideoPlay : MonoBehaviour
{
    public GameObject Panel;
    public VideoPlayer videoPlayer,videoPlayerEnds;
    private string videoUrl = "Assets/VideoPlay/video_kids.mp4";
    private string videoUrl1 = "Assets/VideoPlay/end_video.mp4";
    private double time;
    private double currentTime;
    private int count = 0,countDown=0;
    
    //public Image replay,next;
     
    // Start is called before the first frame update
    void Start()
    {
        Panel.gameObject.SetActive(false);
        videoPlayer.url = videoUrl;
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoPlayer.EnableAudioTrack (0, true);
        videoPlayer.Prepare ();

    }
 
    // Update is called once per frame
    void Update()
    {
        if (( videoPlayer.frame) > 0 && (videoPlayer.isPlaying == false)){
            count++;
            if(count==1){
            Debug.Log ("VIDEO IS OVER");
            videoEnds();
            }
        }  
        if (( videoPlayer.frame) > 0 && (videoPlayer.isPlaying == false)){
            countDown++;
            if(countDown==1){
            Debug.Log ("VIDEO IS OVER");
            Panel.gameObject.SetActive(true);
            videoEnds();
            }
        }
    }

    public void videoEnds(){
        videoPlayerEnds.url = videoUrl1;
        videoPlayerEnds.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoPlayerEnds.EnableAudioTrack (0, true);
        videoPlayerEnds.Prepare ();
    }

    public void replay(){
        Panel.gameObject.SetActive(false);
        videoPlayer.url = videoUrl;
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoPlayer.EnableAudioTrack (0, true);
        videoPlayer.Prepare ();
    }

     public void next(){//ColorMixScenes
        LoadScene("ColorMixScenes");
    }



    private void LoadScene(string targetScene)
    {
       SceneManager.LoadScene(targetScene);
    }

}
